---
title: "Разработка ПО систем управления"
author: Козлюк Д. А.
menuTitle: 2018

course:
  meta:
    year: 2018
    term: весна

  staff:

  - name:
      full: Мохов Андрей Сергеевич
      short: Мохов А. С.
    role: лектор, лаборант
    contacts:
      mail: http://www.google.com/recaptcha/mailhide/d?k=01b8l7FXuXWtJtevPcA3t_YA==&c=pdvcnKCgYeeWQNdargTfYqRxvCc-7y0JPJu1_TptWCQ=
      vk: https://vk.com/id234483

  - name:
      full: Яблоков Иван Олегович
      short: Яблоков И. О.
    role: лаборант
    contacts:
      tg: https://t.me/Lelovki


  groups:
  - А-01-17
  - А-02-17
  - А-03-17


  lectures:
  - topic: "1. Основы языка C++"
    week: 2
    date: "14.02"
    links:
      pdf: lecture01_intro.slides-2018.pdf
      gdrive: https://drive.google.com/open?id=1Y3JkiCD9kVkwa7cGiXqA6HAgdfx9sIlC
    extra: |
      * Раздаточные материалы:
        <a class="pdf icon" href="lecture01_intro.handouts-2018.pdf"></a>
        <a class="gdrive icon" href="https://drive.google.com/open?id=1UQ8TZSub6P-J9Yx4Dlu4k2n9xfpKxBwl"></a>

  - topic: "2. Системы контроля версий"
    week: 4
    date: "28.02"
    links:
      pdf: lecture02_vcs.slides-2018.pdf
      gdrive: https://drive.google.com/open?id=18Y4OZZ9aCYbj1JSQiYYuq-6Yhml-8gVc

  - topic: "3. Структурирование программы и её взаимодействие с пользователем"
    week: 6
    date: "14.03"
    links:
      pdf: lecture03_structured-program.slides-2018.pdf
      gdrive: https://drive.google.com/open?id=13CgcOg3pesSaQmwCTsqzfqRlj-VvOe7T

  - topic: "4. Физическая декомпозиция и контроль корректности программ"
    week: 8
    date: "28.03"
    links:
      pdf: lecture04.1_build-and-test.slides-2018.pdf
      gdrive: https://drive.google.com/open?id=1y4CAhqfl8WPmQ0nV-RgQnAFo87OMaF_F

  - topic: "5. Низкоуровневые средства C++ для работы с памятью"
    week: 10
    date: "11.04"
    links:
      pdf: lecture05_bits.slides-2018.pdf

  - topic: "6. Низкоуровневые средства C++ для работы с памятью (продолжение)"
    week: 12
    date: "25.04"
    links:
      pdf: lecture06_bits-2.slides-2018.pdf


  labs:
  - topic: "1. Среда разработки CLion и основы языка C++"
    dates:
    - "08.02"
    - "06.02"
    - "13.02"
    extra: |
      <a href="lab01_intro.task.html" class="html icon"></a>
      <a href="lab01_intro.task.md.txt" class="markdown icon"></a>

      * Эталонный <a href="02-label-alignment.in.txt" class="text file">ввод</a>
        и <a href="02-label-alignment.out.txt" class="text file">вывод</a>
        после выравнивания подписей.
      * Эталонный <a href="03-scaling.out.txt" class="text file">вывод</a>
        после масштабирования графика (ввод тот же, что и ранее).
      * [Основы работы с командной строкой](http://cmd.readthedocs.io/cmd.html)
      * [Перенаправление стандартных потоков ввода и вывода](http://www.windowsfaq.ru/content/view/260/57/)
      * [О программе FC для сравнения файлов](http://ab57.ru/cmdlist/fc.html)
        (никакие ключи для целей ЛР не нужны).

  - topic: "Защиты ЛР №1 и консультации"
    dates:
    - "22.02"
    - "20.02"
    - "27.02"
    extra: |
      * [Тест перед защитой ЛР № 1](https://goo.gl/Lxprw2)

  - topic: "2. Система контроля версий Git"
    dates:
    - "22.03"
    - "06.03"
    - "13.03"
    extra: |
      <a href="lab02_vcs.task.pdf" class="pdf icon"></a>
      <a href="https://drive.google.com/open?id=1rUFbuf3VDZH_DPsTAWYheGDNuX8SFqw6" class="gdrive icon"></a>

  - topic: "Защиты ЛР №2 и консультации"
    dates:
    - "05.04"
    - "20.03"
    - "27.03"
    extra: |
      * [Тест перед защитой ЛР № 2](https://goo.gl/D9sRbd)

  - topic: "3. Декомпозиция и контроль корректности программ"
    dates:
    - "19.04"
    - "03.04"
    - "10.04"
    extra: |
      <a href="lab03_struct.task.html" class="html icon"></a>
      <a href="lab03_struct.task.md.txt" class="markdown icon"></a>

      * Команды для удобного запуска CTest
        - дома — `doskey ctest="%ProgramFiles(x86)%\JetBrains\CLion2017.1\bin\cmake\bin\ctest" $*`
        - в лаборатории — `doskey ctest="%ProgramFiles(x86)%\JetBrains\bin\cmake\bin\ctest" $*`

  - topic: "Защиты ЛР №3 и консультации"
    dates:
    - "03.05"
    - "17.04"
    - "24.04"
    extra: |
      * [Тест перед защитой ЛР № 3](https://goo.gl/MI1IHA)

  - topic: "4. Представление данных в памяти"
    dates:
    - "17.05"
    - "15.05"
    - "08.05"
    extra: |
      <a href="lab04_bits.task.pdf" class="pdf icon"></a>

      * [Методические указания к ЛР № 4](lab04.html)

  - topic: Дополнительный день защит лабораторных работ
    date: "23.05"
    time: "3—4 пара"
    room: "Ж-211"

  - topic: "Защиты ЛР №4 и консультации"
    dates:
    - "31.05"
    - "29.05"
    - "22.05"
    extra: |
      * [Тест перед защитой ЛР № 4](https://goo.gl/g0Erjq)

  - topic: "Дополнительный день защит лабораторных работ"
    date: "05.06"
    time: "1—2 пара"
    room: "Ж-211"

  - topic: "Дополнительный день защит лабораторных работ. Будет присутствовать только Мохов А.С."
    date: "06.06"
    time: "3—4 пара"
    room: "Ж-211"

  - topic: "Дополнительный день защит лабораторных работ"
    date: "14.06"
    time: "13:00 — 15:00"
    room: "Ж-211"

  - topic: "Дополнительный день защит лабораторных работ. Будет присутствовать только Мохов А.С."
    date: "23.06 (суббота)"
    time: "12:00 — 15:00"
    room: "М-304а"

  books:

    ppp:
      authors: "Bjarne Stroustrup"
      title: "Programming: Principles and Practices Using C++"
      translation:
        authors: "Страуструп Б."
        title: "Программирование: принципы и практика использования C++"

    primer:
      authors: "Stanley Lippmann, Josee Lajoie, Barbara Moo"
      title: "C++ Primer, 5th Edition"
      translation:
        authors: "Липманн С., Лажойе Ж., Му Б."
        title: "Язык программирования С++: вводный курс, изд. 5"

    prata:
      authors: "Стивен Прата"
      title: "Язык программирования C++. Лекции и упражнения"
      edition: "6-е изд."
      publisher: "Спб: Вильямс, 2012"
      pages: "1248 с."

    deitel:
      authors: "Paul & Harvey Deitel"
      title: "C: How to Program, 7th Edition"

    dummy:
      authors: "Стефан Рэнди Дэвис"
      title: "C++ для чайников"
      publisher: "Спб.: Вильямс, 2015"
      pages: "400 с."

    ntb:
      authors: "Подбельский В. В."
      title: "Язык Си++: Учебное пособие"
      edition: "5-е изд."
      publisher: "М.: Финансы и статистика, 1999"
      pages: "560 с.: ил."

---

{{< mirror >}}



## Лекции

{{< lectures >}}



## Лабораторные работы

## [Журнал групп](https://docs.google.com/spreadsheets/d/1iIka-4AyZ80dUJwUWXCEMJfj1YYVi67uJDeUWcneBhU)

* Допуск к устной защите — после успешного прохождения теста.
    Критерий — количество правильных ответов, например, 7 из 10.

* Защита принимается только по наличии распечатанного отчета, включающего:
  * титульный лист;
  * постановку задачи;
  * описание логики решения своего варианта (несколько предложений);
  * весь исходный код (допустим убористый формат);
  * начиная с ЛР № 2, ссылку на репозитарий.

* Начиная с ЛР № 2, код должен быть опубликован в Git
    (на GitHub, BitBucket или подобном, куда у лаборантов есть доступ).

{{< labs >}}



## Литература

В конце каждой лекции указывается, что именно и из каких источников можно
прочесть по теме.

### Язык C++

1. {{< bibref "ppp" >}} \
    Рекомендованный учебник. Перевод на русский имеется только
    для первого издания.
    [Страница книги](http://www.stroustrup.com/Programming/)
    (комментарии, советы, примеры кода).

1. {{< bibref "primer" >}} \
    Популярный учебник, предполагающий уверенное владение каким-либо еще языком программирования, помимо С++.

    Печатные экземпляры есть в М-304а.

1. {{< bibref "prata" >}} \

1. {{< bibref "deitel" >}} \

1. {{< bibref "dummy" >}} \
    Местами поверхностное и неточное, но емкое и крайне доходчивое изложение.

1. {{< bibref "ntb" >}} \
    Существенно устаревшая книга, но только она массово **есть в НТБ МЭИ (УДК: П44).**


### Электронные ресурсы

* Сайт [C++ Reference](http://cppreference.com/) —
    неофициальный, но подробный справочник, достаточно авторитетный в профессиональных кругах.
* [Конспект лекций по C и C++](http://natalia.appmat.ru/c&c++/)
    доцента кафедры Прикладной математики МЭИ(ТУ) Натальи Владимировны Чибизовой.


### Системы контроля версий

* [Pro Git](http://git-scm.com/book) —
    бесплатная элекронная книга-руководство по Git.
* [Hg Init: a Mercurial Tutorial](http://hginit.com/) —
    набор пошаговых обучающих статей по Mercurial.
* [The Git Parable](http://tom.preston-werner.com/2009/05/19/the-git-parable.html) —
    статья, объясняющая логику построения Git.
* [Subversion и разработка web-приложений](http://edu.cbias.ru/Subversion.pdf) —
    презентация об основах VCS и применении Subversion для командной разработки
    (Фетисовы Н. А. и Ф. А., 2012).



## Программное обеспечение


### Средства разработки, как в лаборатории

* [MinGW-w64 и CLion](/study/courses/common/clion.html) — компилятор и IDE.
* [Git и TortoiseGit](/study/courses/common/git.html) — система контроля версий и GUI для Windows.
* [CMake](https://cmake.org/download/) — система сборки программ (см. Binary Distributions).


### Online-службы

* [Ideone.com](https://ideone.com) — бесплатный online компилятор.
    Удобен для быстрой проверки небольших программ и обмена примерами.
* [GitHub](https://github.com) — самый крупный и популярный хостинг Git. Бесплатно предоставляются
    только открытые хранилища (то есть видимые для всех).
    Обладает обширной инфраструктурой и развитым web-интерфейсом.
* [BitBucket](https://bitbucket.org) — хостинг Git и Mercurial (Hg), бесплатно предоставляет открытые
    и частные хранилища. Дополнительных возможностей меньше, чем на GitHub.


## Преподаватели

{{< staff >}}
